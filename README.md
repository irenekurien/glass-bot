# Glass Bot

A svelte app for the frontend of Glass Chatbot Set Up

### Getting Started

```
git clone git@gitlab.com:irenekurien/glass-bot.git
cd glass-bot
npm install
npm run dev
```
