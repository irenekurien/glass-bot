import Home from './routes/Home.svelte';
import Login from './routes/Login.svelte';
import Products from './routes/Products.svelte';
import NotFound from './routes/NotFound.svelte';
import Delete from './routes/Delete.svelte';
import ViewProduct from './routes/ViewProduct.svelte';
import MenuResponse from './routes/MenuResponse.svelte';
import ProductGreeting from './routes/Product_Greeting.svelte';

export default {
	'/': Home,
	'/login': Login,
	'/products': Products,
	'/product/:pid': ViewProduct,
	'/add-product': ProductGreeting,
	// '/product-details': ,
	'/menu': MenuResponse,
	'/delete-product': Delete,
	// The catch-all route must always be last
	'*': NotFound,
};
