import { writable } from 'svelte/store';

export const User = (() => {
	const { subscribe, set } = writable(null);
	return {
		subscribe,
		token: '',
		isLoggedIn: false,
		login: (_token) => {
			set(_token);
			loginHandler(_token);
		},
		logout: () => {
			set(null);
			logoutHandler();
		},
	};
})();

const loginHandler = (_token) => {
	if (_token !== undefined) {
		User.token = _token;
		User.isLoggedIn = true;
		localStorage.setItem('token', _token);
	}
};

const logoutHandler = () => {
	User.token = '';
	User.isLoggedIn = false;
	localStorage.removeItem('token');
};
