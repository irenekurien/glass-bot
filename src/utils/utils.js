export function encodeQuery(data) {
	let query = '';
	for (let d in data)
		query += encodeURIComponent(d) + '=' + encodeURIComponent(data[d]) + '&';
	return query.slice(0, -1);
}

export function isEmpty(val) {
	return val.trim().length === 0;
}

export function isHexVaild(val) {
	return /^#([0-9A-F]{3}){1,2}$/i.test(val);
}
