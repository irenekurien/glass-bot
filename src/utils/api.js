import { encodeQuery } from './utils';

const base = 'https://trialbot.getglass.co';


async function send({ method, path, data, type }) {
	const opts = { method, headers: {} };

	if (data) {
		if (type === 'formData') {
			const formData = new FormData();
			for (let key in data) {
				formData.append(key, data[key]);
			}
			opts.body = formData;
		} else if (type === 'query') {
			path = `${path}?${encodeQuery(data)}`;
		} else {
			opts.headers['Content-Type'] = 'application/json';
			opts.body = JSON.stringify(data);
		}
	}

	const token = localStorage.getItem('token');

	if (token) {
		opts.headers.Authorization = `Bearer ${token}`;
	}

	const fullPath = encodeURI(`${base}/${path}`);

	const response = await fetch(fullPath, opts);
	const json = await response.json();

	return { response, json };
}

/*
 * Shortcut methods for send
 */
export function get(path) {
	return send({ method: 'GET', path, type: 'formData' });
}

export function del(path, data, type) {
	return send({ method: 'DELETE', path, data, type });
}

export function post(path, data, type) {
	return send({ method: 'POST', path, data, type });
}

export function put(path, data, type) {
	return send({ method: 'PUT', path, data, type });
}

export function getAbsPath(relativePath) {
	return base + relativePath;
}